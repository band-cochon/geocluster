FROM ubuntu:22.10 as builder

WORKDIR /home/build

COPY src src
COPY CMakeLists.txt .
COPY conanfile.txt .

RUN apt update && \
    apt install -y build-essential cmake python3-pip --install-recommends && \
    pip3 install conan && \
    conan install . --build=missing && \
    cmake . && \
    cmake --build . -j

FROM ubuntu:22.10
WORKDIR /app
COPY --from=builder --chown=root:root /home/build/bin/geocluster /app/geocluster
COPY config.ini .

ENV DEBUG=0
EXPOSE 5000

CMD [ "/app/geocluster", "-c", "/app/config.ini" ]
